package main

import (
	"github.com/gin-gonic/gin"
	"strconv"
)

func isPrime(n uint64) bool {
	var x uint64 = 2
	for x < n {
		if n%x == 0 {
			return false
		}
		x++
	}
	return true
}

func next_prime(n uint64) uint64 {
	var val uint64 = n + 1
	for {
		if isPrime(val) {
			return val
		} else {
			val++
		}
	}
}

func factors(n uint64) []uint64 {
	factor_list := make([]uint64, 0)
	var prime uint64 = 1
	for next_prime(prime) <= n {
		for n%next_prime(prime) == 0 {
			n /= next_prime(prime)
			factor_list = append(factor_list, next_prime(prime))
		}
		prime = next_prime(prime)
	}
	return factor_list
}

func main() {
	r := gin.Default()
	//listen for get on /factorize
	r.GET("/factorize", func(c *gin.Context) {
		//get query param number as a string
		stringToFactorize := c.Query("number")

		//convert string to uint64
		inToFactorize, err := strconv.ParseUint(stringToFactorize, 10, 64)

		if err != nil {
			c.JSON(400, "not a number")
		} else {
			c.JSON(200, factors(inToFactorize))
		}
	})
	r.Run() // listen and serve on 0.0.0.0:8080
}
