Description
===

This api will respond on /factorize

You need to set the argument number to have a result, which will be in json format

Example **http://localhost:8080/factorize?number=18** will return **"[2,3,3]"** with application/json type.


Rules / Exceptions
===

If the number is bigger than a uint64 (18446744073709551615) an error will be thrown

If the argument is not a number or if empty/not set, an error will be thrown (400)

If the wrong path is targeted, 404 is thrown

Build
===
If you have go installed on your laptop, you can build with **./buildFromLaptop.sh** (recommended, so it doesn't download a heavy image)

If not, use **./buildFromDocker.sh** (it will use multi stage docker build with official docker image for golang). Resulting image will be a much lighter image than the official image, using alpine as a base and a static binary inside. 

If you want to run it on your laptop directly, use **make** 

Clean
===

to clean either remove main binary, or run make clean 

Run
===

To run the container: ./run.sh portNumber

Example: **./run.sh 8080**

Then target the api on http://localhost:portNumber/factorize?number=number 

Others
===

gin gonic handle concurrency with goroutines (green threading).

No timeout is set, so it will wait until the factors function is done. Unless the number provided is not valid.
