default : static

static :
	go get
	CGO_ENABLED=0 go build -o main main.go

notstatic : 
	go get
	go build -o main

clean :
	rm ./main
