FROM golang
COPY main.go /
RUN go get github.com/gin-gonic/gin  \
    && CGO_ENABLED=0 GOOS=linux go build -o /main /main.go

FROM alpine:latest  
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=0 /main .
CMD ["./main"]  

